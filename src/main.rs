// File Name: main.rs
// Description: NetCDF info tool
// Author: KMIJPH
// Repository: https://codeberg.org/KMIJPH
// License: GPLv3+
// Creation Date: Sun 06 Aug 2022 02:08:30 PM CEST
// Last Modified: 13 Mar 2023 15:58:26

mod nc;

use nc::file::File;
use std::fmt::Write;

fn print_help() -> String {
    let mut buf = String::new();
    write!(buf, "USAGE: {} [flags] <file>\n", env!("CARGO_PKG_NAME")).unwrap();
    write!(
        buf,
        "  For more help visit: {}\n",
        env!("CARGO_PKG_REPOSITORY")
    )
    .unwrap();
    buf.push_str("FLAGS:\n");
    buf.push_str("  --help -h      show this message\n");
    buf.push_str("  --version -v   show version");
    return buf;
}

fn main() {
    let arg = std::env::args().nth(1).expect(&print_help());

    match arg.as_str() {
        "--help" | "-h" => {
            println!("{}", print_help());
            std::process::exit(0)
        }
        "--version" | "-v" => {
            println!(env!("APP_VERSION"));
            std::process::exit(0)
        }
        _ => {
            let netcdf_file = File::new(&arg);

            match netcdf_file {
                Ok(n) => {
                    println!("{}", n)
                }
                Err(e) => {
                    println!("ERROR: Could not get information: {:?}", e);
                    std::process::exit(1)
                }
            }
        }
    }
}
