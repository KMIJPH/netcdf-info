// File Name: variable.rs
// Description: Netcdf attribute/dimension/variable
// Author: KMIJPH
// Repository: https://codeberg.org/KMIJPH
// License: GPLv3+
// Creation Date: 31 Oct 2022 15:02:23
// Last Modified: 31 Oct 2022 15:16:49

use std::string;
use textwrap::indent;

// NetCDF Variable attribute
pub struct VarAttribute {
    name: String,
    val: String,
}

impl VarAttribute {
    pub fn new(name: String, val: String) -> Self {
        return Self { name, val };
    }
}

impl string::ToString for VarAttribute {
    fn to_string(&self) -> String {
        return format!("{}: {}", self.name, self.val);
    }
}

// NetCDF Dimension
pub struct Dimension {
    name: String,
    len: usize,
}

impl Dimension {
    pub fn new(name: String, len: usize) -> Self {
        return Self { name, len };
    }
}

impl string::ToString for Dimension {
    fn to_string(&self) -> String {
        return format!("{}: {}", self.name, self.len);
    }
}

// NetCDF Attribute
pub struct Attribute {
    name: String,
    value: String,
}

impl Attribute {
    pub fn new(name: String, value: String) -> Self {
        return Self { name, value };
    }
}

impl string::ToString for Attribute {
    fn to_string(&self) -> String {
        return format!("{}: {}", self.name, self.value);
    }
}

// NetCDF Variable
pub struct Variable {
    attrs: Vec<VarAttribute>,
    name: String,
    len: usize,
    typ: String,
    dims: Vec<Dimension>,
}

impl Variable {
    pub fn new(
        attrs: Vec<VarAttribute>,
        name: String,
        len: usize,
        typ: String,
        dims: Vec<Dimension>,
    ) -> Self {
        return Self {
            attrs,
            name,
            len,
            typ,
            dims,
        };
    }
}

impl string::ToString for Variable {
    fn to_string(&self) -> String {
        let mut attrs: String = self
            .attrs
            .iter()
            .map(|x| x.to_string())
            .collect::<Vec<String>>()
            .join("\n");
        let mut dims: String = self
            .dims
            .iter()
            .map(|x| x.to_string())
            .collect::<Vec<String>>()
            .join("\n");

        attrs = indent(&format!("\n{}", attrs), "    ");
        dims = indent(&format!("\n{}", dims), "    ");

        format!(
            "Name: {}\nLength: {}\nType: {}\nAttributes: {}\nDimensions: {}",
            self.name, self.len, self.typ, attrs, dims,
        )
    }
}
