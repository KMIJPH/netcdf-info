// File Name: mod.rs
// Description: Netcdf module
// Author: KMIJPH
// Repository: https://codeberg.org/KMIJPH
// License: GPLv3+
// Creation Date: 31 Oct 2022 15:04:22
// Last Modified: 31 Oct 2022 15:05:00

pub mod file;
mod variable;
