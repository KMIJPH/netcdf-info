// File Name: file.rs
// Description: NetCDF file information
// Author: KMIJPH
// Repository: https://codeberg.org/KMIJPH
// License: GPLv3+
// Creation Date: Sun 06 Aug 2022 02:08:30 PM CEST
// Last Modified: 31 Oct 2022 15:17:50

use super::variable::{Attribute, Dimension, VarAttribute, Variable};

use netcdf;
use std::fmt;
use textwrap::indent;

// separator (works for now)
const SEP: &str = "----------------";

// NetCDF File
pub struct File {
    attrs: Vec<Attribute>,
    name: String,
    vars: Vec<Variable>,
}

impl File {
    // returns new NetCDF file information struct
    pub fn new(file: &String) -> Result<Self, Box<dyn std::error::Error>> {
        let vars = get_variables(file)?;
        let attrs = get_attributes(file)?;

        return Ok(Self {
            attrs,
            name: file.to_string(),
            vars,
        });
    }
}

impl fmt::Display for File {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut attrs: String = self
            .attrs
            .iter()
            .map(|x| x.to_string())
            .collect::<Vec<String>>()
            .join("\n");
        let mut vars: String = self
            .vars
            .iter()
            .map(|x| x.to_string())
            .collect::<Vec<String>>()
            .join(&format!("\n{}\n", SEP));

        vars = indent(&vars, "  ");
        attrs = indent(&attrs, "  ");

        write!(
            f,
            "Name: {}\nVariables:\n  {}\n{}\n  {}\nAttributes:\n{}",
            self.name, SEP, vars, SEP, attrs
        )
    }
}

// returns all variables found in a NetCDF file
fn get_variables(file: &String) -> Result<Vec<Variable>, Box<dyn std::error::Error>> {
    let netcdf = netcdf::open(file.as_str())?;
    let mut vars = Vec::new();

    for i in netcdf.variables() {
        let name = i.name();
        let len = i.len();
        let typ = i.vartype().name();

        let dims: Vec<Dimension> = i
            .dimensions()
            .iter()
            .map(|x| {
                let name = x.name();
                let len = x.len();
                Dimension::new(name, len)
            })
            .collect();

        let attrs: Vec<VarAttribute> = i
            .attributes()
            .map(|x| {
                let name = x.name().to_string();
                let res = x.value();
                let val = match res {
                    Ok(v) => match v {
                        netcdf::AttrValue::Str(w) => w,
                        netcdf::AttrValue::Float(w) => w.to_string(),
                        netcdf::AttrValue::Short(w) => w.to_string(),
                        netcdf::AttrValue::Int(w) => w.to_string(),
                        netcdf::AttrValue::Double(w) => w.to_string(),
                        netcdf::AttrValue::Longlong(w) => w.to_string(),
                        _ => format!("{:?}", v),
                    },
                    Err(e) => e.to_string(),
                };
                VarAttribute::new(name, val)
            })
            .collect();

        let result = Variable::new(attrs, name, len, typ, dims);
        vars.push(result);
    }
    return Ok(vars);
}

// returns all attributes found in a NetCDF file
fn get_attributes(file: &String) -> Result<Vec<Attribute>, Box<dyn std::error::Error>> {
    let netcdf = netcdf::open(file.as_str())?;
    let mut attrs = Vec::new();

    for a in netcdf.attributes() {
        let name = a.name().to_string();
        let val = a.value();

        let value = match val {
            Ok(v) => match v {
                netcdf::AttrValue::Str(w) => w,
                netcdf::AttrValue::Float(w) => w.to_string(),
                netcdf::AttrValue::Short(w) => w.to_string(),
                netcdf::AttrValue::Int(w) => w.to_string(),
                netcdf::AttrValue::Double(w) => w.to_string(),
                netcdf::AttrValue::Longlong(w) => w.to_string(),
                _ => format!("{:?}", v),
            },
            Err(e) => e.to_string(),
        };

        attrs.push(Attribute::new(name, value))
    }
    return Ok(attrs);
}
