<!---
title: netcdf-info
description: netcdf-info readme
author: KMIJPH
created: 29 Oct 2022 10:42:30
last_modified: 22 May 2023 20:43:38
-->


# netcdf-info

CLI tool to print information about a NetCDF file.  
Used primarily to preview information for files in terminal filemanagers.

## Usage

```
USAGE: netcdf-info [flags] <file>
  For more help visit: https://codeberg.org/KMIJPH/netcdf-info
FLAGS:
  --help -h      show this message
  --version -v   show version
```

## Building

```bash
cargo build --release
```
