// File Name: build.rs
// Description: Build script to add version env var
// Author: KMIJPH
// Repository: https://codeberg.org/KMIJPH
// License: GPLv3+
// Creation Date: 29 Oct 2022 12:12:37
// Last Modified: 16 Nov 2023 12:09:48

use std::process::Command;

fn main() {
    let version = env!("CARGO_PKG_VERSION");
    let output = Command::new("git")
        .args(&["rev-parse", "--short", "HEAD"])
        .output();
    match output {
        Ok(out) => println!(
            "cargo:rustc-env=APP_VERSION={}-{}",
            version,
            String::from_utf8(out.stdout).unwrap()
        ),
        Err(_) => println!("cargo:rustc-env=APP_VERSION={}-commit", version),
    }
}
