{ pkgs ? import <nixpkgs> { } }:
# nix-channel 23.11
let nixPackages = with pkgs; [ cargo rustc cmake ];
in pkgs.mkShell { nativeBuildInputs = nixPackages; }
